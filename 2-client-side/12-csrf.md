---
title: 'Cross-Site Request Forgery (CSRF)'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# CSRF vulnerability with no defenses

<https://portswigger.net/web-security/csrf/lab-no-defenses>

Completed: 2022-12-08


## Summary

TKTK


## Objective

> This lab's email change functionality is vulnerable to CSRF.
>
>> To solve the lab, craft some HTML that uses a CSRF attack to change the viewer's email address and upload it to your exploit server.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. Update email address
1. Right click the POST request > Engagement tools > Generate CSRF PoC
1. Go to "Options" and tick "Include auto-submit script", then click "Regenerate"
1. Paste the PoC code into the "Body" field of the provided exploit server, click "Store"
1. Click "Deliver exploit to victim"




# Bypass token validation: CSRF where token validation depends on request method

<https://portswigger.net/web-security/csrf/bypassing-token-validation/lab-token-validation-depends-on-request-method>

Completed: 2022-12-09


## Summary

TKTK

The web app prevents CSRF via POST requests but not via GET requests.


## Objective

> This lab's email change functionality is vulnerable to CSRF. It attempts to block CSRF attacks, but only applies defenses to certain types of requests.
>
>> To solve the lab, use your exploit server to host an HTML page that uses a CSRF attack to change the viewer's email address.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. Change email
1. Take the POST request in Repeater, change it to GET:

    ```
	POST /my-account/change-email HTTP/1.1
	```

	... becomes...
	
    ```
	GET /my-account/change-email?email=pwned@evil-user.net HTTP/1.1
	```

	If sending the request without the `?email` parameter, the response explains `"Missing parameter 'email'"`

1. Now right click on the GET request > Engagement tools > Generate CSRF PoC
1. Click "Options" > "Include auto-submit script". Now click "Regenerate"
1. Copy the code and paste into the "Body" field of the exploit server
1. Click "Deliver exploit to victim"

    The web app prevents CSRF via POST requests but not via GET requests.




# Bypass token validation: CSRF where token validation depends on token being present

<https://portswigger.net/web-security/csrf/bypassing-token-validation/lab-token-validation-depends-on-token-being-present>

Completed: 2022-12-09


## Summary

TKTK

The web app prevents CSRF successfully using the `csrf` token, but fails open if the token is not present in the request.


## Objective

> This lab's email change functionality is vulnerable to CSRF.
>
>> To solve the lab, use your exploit server to host an HTML page that uses a CSRF attack to change the viewer's email address.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. Update email address
1. Right click the POST request > Engagement tools > Generate CSRF PoC
1. Delete the `csrf` parameter entirely
1. Go to "Options" and tick "Include auto-submit script", then click "Regenerate"
1. Paste the PoC code into the "Body" field of the provided exploit server, click "Store"
1. Click "Deliver exploit to victim"

    The web app prevents CSRF successfully using the `csrf` token, but fails open if the token is not present in the request.




# Bypass token validation: CSRF where token is not tied to user session

<https://portswigger.net/web-security/csrf/bypassing-token-validation/lab-token-not-tied-to-user-session>

Completed: TKTK




# Bypass token validation: CSRF where token is tied to non-session cookie

<https://portswigger.net/web-security/csrf/bypassing-token-validation/lab-token-tied-to-non-session-cookie>

Completed: TKTK




# Bypass token validation: CSRF where token is duplicated in cookie

<https://portswigger.net/web-security/csrf/bypassing-token-validation/lab-token-duplicated-in-cookie>

Completed: TKTK




# Bypass SameSite cookie restrictions: SameSite Lax bypass via method override

<https://portswigger.net/web-security/csrf/bypassing-samesite-restrictions/lab-samesite-lax-bypass-via-method-override>

Completed: TKTK




# Bypass SameSite cookie restrictions: SameSite Strict bypass via client-side redirect

<https://portswigger.net/web-security/csrf/bypassing-samesite-restrictions/lab-samesite-strict-bypass-via-client-side-redirect>

Completed: TKTK




# Bypass SameSite cookie restrictions: SameSite Strict bypass via sibling domain

<https://portswigger.net/web-security/csrf/bypassing-samesite-restrictions/lab-samesite-strict-bypass-via-sibling-domain>

Completed: TKTK




# Bypass SameSite cookie restrictions: SameSite Lax bypass via cookie refresh

<https://portswigger.net/web-security/csrf/bypassing-samesite-restrictions/lab-samesite-strict-bypass-via-cookie-refresh>

Completed: TKTK




# Bypass Referer-based defences: CSRF where Referer validation depends on header being present

<https://portswigger.net/web-security/csrf/bypassing-referer-based-defenses/lab-referer-validation-depends-on-header-being-present>

Completed: TKTK




# Bypass Referer-based defences: CSRF with broken Referer validation

<https://portswigger.net/web-security/csrf/bypassing-referer-based-defenses/lab-referer-validation-broken>

Completed: TKTK

