---
title: 'Topic'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Lab A

Completed: 2023-01-01


## Objective

> paste lab intro here


## Summary

X was vulnerable to Y. Exploit by doing Z.


## Solution

1. step one
1. step two




# Example features


## sub-headings


### sub-sub-headings, etc...


### images

![example image, default width](./images/academy-logo.svg)

![example image](./images/academy-logo.svg){width=15cm}


### links {#custom-id}

<https://portswigger.net/>

[Pretty URL](https://portswigger.net/)

[Link to auto heading ID](#example-features)

[Anchor link to custom heading ID](#custom-id)

[Reference link][1]

[1]: https://portswigger.net/ 'PortSwigger'


### font

Line  
breaks

**bold**

*italicised*

***bold and italicised***

~~strikethrough~~

~sub~script

^super^script


### code

Inline `code` here is a test

```plain {.numberLines startFrom=1}
fenced code block with
syntax highlighting and line numbers
```


### blockquotes

> blockquotes
>
>> nested blockquotes


### lists

1. ordered lists
1. ordered lists

- unordered lists
- unordered lists

- [x] task lists
- [ ] task lists

Definition lists
: definition
: more definition

Definition lists
: definition
: more definition


### miscellaneous

Horizontal rules:

---

Pipe tables:

| col 1 | col 2 | col 3 |
| :---- | :---: | ----: |
| row 1 | row 1 | row 1 |
| row 2 | row 2 | row 2 |

