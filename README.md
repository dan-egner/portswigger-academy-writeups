---
title: 'README'
subtitle: 'PortSwigger Web Security Academy Lab Writeups'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: false
toc: false
---

## Description

My writeups of the [PortSwigger Web Security Academy](https://portswigger.net/web-security) labs.

The writeups are divided based on topic. Each topic is numbered according to the suggested [Learning Path](https://portswigger.net/web-security/learning-path).

This is a work in progress as I continue to complete new labs.




## [Optional] Convert Markdown to PDF

If on Linux, I recommend using [Pandoc](https://pandoc.org/) with command line options similar to those below.

This process can be automated using a shell script. The script can be automatically run by your text editor when writing to files with Markdown-related file extensions.


```sh
$ pandoc \
    --defaults </path/to/custom.yaml> \
    -f markdown-implicit_figures-raw_tex \
    -o </path/to/outputfile.pdf> </path/to/inputfile.md> &

# pandoc markdown extension 'implicit_figures' converts figures to inline images, no captions, fixed position
# pandoc markdown extension 'raw_tex' allows unescaped backslashes
```


### custom.yaml file

```yaml
pdf-engine: xelatex
highlight-style: </path/to/dracula.theme>


variables:

  geometry:  margin=2cm
  numbersections: true

  mainfont: DejaVuSerif
  mainfontoptions: Extension=.ttf, UprightFont=*, BoldFont=*-Bold, ItalicFont=*-Italic, BoldItalicFont=*-BoldItalic
  sansfont: DejaVuSans.ttf
  #monofont: DejaVuSansMono.ttf

  linkcolor: draculapink
  filecolor: draculagreen
  citecolor: draculapurple
  urlcolor:  draculacyan
  toccolor:  draculaorange

  header-includes:

    # use .sty file to define colours, set bg and fg colours
    - \usepackage{</path/to/draculatheme>}

    # break long lines in fenced code blocks
    - \usepackage{fvextra}
    - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,breakanywhere,commandchars=\\\{\}}
    - \renewcommand{\NormalTok}[1]{\FancyVerbBreakStart#1\FancyVerbBreakStop}

    # set default max image width, will not scale above 100%
    - \usepackage{graphicx}
    - \setkeys{Gin}{width=\linewidth}

    # add background colour to inline code, same bg colour as code blocks
    - \let\oldtexttt\texttt
    - \renewcommand{\texttt}[1]{
        \colorbox{codebg}{\oldtexttt{\color{draculagreen}{#1}}}
      }

    # draw coloured bar and background for blockquotes
    - \usepackage[most]{tcolorbox}
    - \newtcolorbox{myquote}[1][]{
        enhanced,
        breakable,
        size=minimal,
        left=10pt,
        top=5pt,
        frame hidden,
        boxrule=0pt,
        sharp corners=all,
        colback=draculacl,
        borderline west={2pt}{0pt}{draculapurple},
        coltext=draculafg,#1}
    - \renewenvironment{quote}{\begin{myquote}}{\end{myquote}}

    # change section heading colour
    - \usepackage{sectsty}
    - \allsectionsfont{\color{draculayellow}}

```


### dracula.theme file

`dracula.theme` is the same as the [original file](https://github.com/dracula/pandoc/blob/master/dracula.theme), except I altered the `"background-color"` value to `"#16171d"` to improve readability of code blocks.


### draculatheme.sty file

`draculatheme.sty` is the same as the [original file](https://github.com/dracula/latex/blob/master/draculatheme.sty), except I added `\definecolor{codebg} {RGB} {22,   23,   29}` to allow inline code to use the same improved background colour as the code blocks.




### Acknowledgements

With thanks to [Calin Leafshade](https://www.youtube.com/@LeafshadeSoftware) for the inspiration for this approach to note taking. [This video](https://www.youtube.com/watch?v=zB_3FIGRWRU) details their setup.

With thanks to [jdhao](https://jdhao.github.io/) and their [blog post](https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/) explaining various improvements which can be made to the Pandoc conversion process.

With thanks to the [Dracula Theme](https://draculatheme.com/), in particular their [Pandoc](https://github.com/dracula/pandoc) and [LaTeX](https://github.com/dracula/latex) themes, for making my PDFs more colourful and readable.

