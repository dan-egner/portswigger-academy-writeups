---
title: 'Business Logic Vulnerabilities'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Excessive trust in client-side controls

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-excessive-trust-in-client-side-controls>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab doesn't adequately validate user input. You can exploit a logic flaw in its purchasing workflow to buy items for an unintended price.
>
>> To solve the lab, buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Go to the store page for "Lightweight l33t leather jacket"
1. Click "Add to cart" and intercept the POST request
1. Change the `price` parameter value to something less than or equal to 10000 (`wiener`'s store credit)
1. Forward the request. The price in the basket is whatever you set it to be
1. Checkout




# Excessive trust in client-side controls: 2FA broken logic

See [authentication writeups](./02-authentication.md.pdf).




# Failure to handle unconventional input: High-level logic vulnerability

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-high-level>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab doesn't adequately validate user input. You can exploit a logic flaw in its purchasing workflow to buy items for an unintended price.
>
>> To solve the lab, buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Go to any item's store page except for the "l33t jacket"
1. Intercept the "Add to cart" POST request
1. Edit the `quantity` parameter value to a negative number and forward the request
1. The total basket price is negative. Attempting to checkout informs us that the total basket price must not be negative
1. Add one "l33t jacket" to the cart in the normal manner
1. If necessary, add a sufficient quantity of other (positively or negatively priced) items to the basket to make the total cost positive, yet lower than `wiener`'s store credit of $100.00
1. Checkout




# Failure to handle unconventional input: Low-level logic flaw

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-low-level>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab doesn't adequately validate user input. You can exploit a logic flaw in its purchasing workflow to buy items for an unintended price.
>
>> To solve the lab, buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Use Burp Intruder to discover that the most items we can add to cart in one request is 99
1. Set Resource Pool to use Max Concurrent Requests of 1
1. Use Burp Intruder (with null payloads) to add `161 * 99` "l33t jackets" to the cart, then manually add some more to bring the quantity up to `16061`. The total price is `$21473557.00`
1. Add one more jacket. The total price flips sign to `-$21474778.96`
1. This integer overflow occurs because the total price exceeds the max possible integer in the programming language used: `133700 * 16062 > 2147483647`
1. Reset the cart, then repeat the above Intruder attack, sending `323` payloads of `99` jackets each. Then add `47` more jackets. The price is now negative and close to zero. One more jacket makes the price positive but greater than our store credit of `$100`
1. Add enough other items (not jackets) to make the total price positive, but below our store credit
1. Checkout




# Failure to handle unconventional input: Inconsistent handling of exceptional input

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-inconsistent-handling-of-exceptional-input>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab doesn't adequately validate user input. You can exploit a logic flaw in its account registration process to gain access to administrative functionality.
>
>> To solve the lab, access the admin panel and delete user `carlos`.


## Solution

1. Register a user account
1. Enter a very long string in the email address field, ending it with our valid mail server address
1. Complete the registration process by clicking the link in the email
1. Log in. Notice that, on the user home page, the email address was truncated to 255 characters
1. Log out
1. `@dontwannacry.com` is 17 characters long. `255 - 17 = 238`, so the malicious input must start with 238 arbitrary characters, immediately followed by `@dontwannacry.com`, which is itself followed by the attacker email server address:

    <aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@dontwannacry.com.exploit-0a1e003404f19539c0fe335d018000af.exploit-server.net>

1. Register a new user, using the above input in the email address field
1. Complete the registration process by clicking the link in the email
1. Log in. The user's email, as displayed on the home page, is truncated to just the `aaa...aaa@dontwannacry.com` part
1. We now have access to the admin panel
1. Delete `carlos`




# Flawed assumptions about user behaviour: Inconsistent security controls

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-inconsistent-security-controls>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab's flawed logic allows arbitrary users to access administrative functionality that should only be available to company employees.
>
>> To solve the lab, access the admin panel and delete the user `carlos`.


## Solution

1. Register a user using the email address of our simulated email client
1. Note that the text on the registration page suggests users with `@dontwannacry.com` email addresses get access to different functionality. We can't register with one of these email addresses as we don't have access to one to complete the registration process
1. Click the link in our email client to compete the registration process
1. Log in as the new user
1. Use the "Change email" functionality on the "My account" page to change to a `@dontwannacry.com` address
1. The link to the "Admin panel" now appears on the page
1. Open the "Admin panel" page and delete `carlos`




# Flawed assumptions about user behaviour: Weak isolation on dual-use endpoint

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-weak-isolation-on-dual-use-endpoint>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab makes a flawed assumption about the user's privilege level based on their input. As a result, you can exploit the logic of its account management features to gain access to arbitrary users' accounts.
>
>> To solve the lab, access the `administrator` account and delete the user `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. Go through the change password functionality
1. The POST request contains a `username` parameter, as well as the `current-password` and new/confirmation password parameters
1. Intercept the request, changing username to `administrator` and deleting the `current-password` parameter/value pair entirely. Forward the request
1. The password of the `administrator` account was changed successfully
1. Log in with the new password and delete `carlos`




# Flawed assumptions about user behaviour: Password reset broken logic

See [authentication writeups](./02-authentication.md.pdf).




# Flawed assumptions about user behaviour: 2FA simple bypass

See [authentication writeups](./02-authentication.md.pdf).




# Flawed assumptions about user behaviour: Insufficient workflow validation

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-insufficient-workflow-validation>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab makes flawed assumptions about the sequence of events in the purchasing workflow.
>
>> To solve the lab, exploit this flaw to buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. Add a cheap item to the cart and checkout
1. Send the "order confirmed" GET request to Burp Repeater:

    ```
	GET /cart/order-confirmation?order-confirmed=true HTTP/1.1
    ```

1. Now add the "l33t jacket" to the cart but don't checkout
1. Send the above GET request from Repeater
1. Refresh the cart page. The leather jacket has been removed because it was successfully purchased, without the cost being applied to our store credit




# Flawed assumptions about user behaviour: Authentication bypass via flawed state machine

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-authentication-bypass-via-flawed-state-machine>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab makes flawed assumptions about the sequence of events in the login process.
>
>> To solve the lab, exploit this flaw to bypass the lab's authentication, access the admin interface, and delete the user `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`
1. At the `/role-selector` page, try to visit `/admin`. Doesn't allow us access
1. Log out
1. Log in again, this time using Burp Proxy intercept to drop the `GET /role-selector` request
1. Then navigate to `/admin`. It now works
1. Delete `carlos`




# Domain-specific flaws: Flawed enforcement of business rules

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-flawed-enforcement-of-business-rules>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab has a logic flaw in its purchasing workflow.
>
>> To solve the lab, exploit this flaw to buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. At top of page, see discount code: `NEWCUST5`
1. At bottom of main store page, enter an email address to reveal discount code: `SIGNUP30`
1. Add the "l33t jacket" to cart
1. Cannot apply the same discount code twice in a row
1. Can apply the two codes over and over again, alternating between the two
1. Apply enough discounts to be able to afford the jacket
1. Checkout




# Domain-specific flaws: Infinite money logic flaw

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-infinite-money>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab has a logic flaw in its purchasing workflow.
>
>> To solve the lab, exploit this flaw to buy a "Lightweight l33t leather jacket".
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

TKTK


You can apply a 30% discount code to orders of gift cards, then redeem the gift cards on your own account, giving you store credit for free.




# Authentication bypass via encryption oracle

<https://portswigger.net/web-security/logic-flaws/examples/lab-logic-flaws-authentication-bypass-via-encryption-oracle>

Completed: 2022-12-07


## Summary

TKTK


## Objective

> This lab contains a logic flaw that exposes an encryption oracle to users.
>
>> To solve the lab, exploit this flaw to gain access to the admin panel and delete the user `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

TKTK

