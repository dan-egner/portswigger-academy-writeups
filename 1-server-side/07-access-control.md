---
title: 'Access Control'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Unprotected admin functionality

<https://portswigger.net/web-security/access-control/lab-unprotected-admin-functionality>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab has an unprotected admin panel.
>
>> Solve the lab by deleting the user `carlos`.


## Solution

1. Visit the `/robots.txt` page. It contains:

    ```
	User-agent: *
	Disallow: /administrator-panel
    ```

1. Visit the `/administrator-panel` page
1. Use the `Delete` button to delete user `carlos`




# Unprotected admin functionality with unpredictable URL

<https://portswigger.net/web-security/access-control/lab-unprotected-admin-functionality-with-unpredictable-url>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab has an unprotected admin panel. It's located at an unpredictable location, but the location is disclosed somewhere in the application.
>
>> Solve the lab by accessing the admin panel, and using it to delete the user `carlos`.


## Solution

1. Visit the main page of the application
1. In Burp's site map, notice that a GET request was made for `/admin-g3r4bx`
1. This was found by Burp because the source code for the main page contains the following JavaScript:

    ```java
	<script>
		var isAdmin = false;
		if (isAdmin) {
		   var topLinksTag = document.getElementsByClassName("top-links")[0];
		   var adminPanelTag = document.createElement('a');
		   adminPanelTag.setAttribute('href', '/admin-g3r4bx');
		   adminPanelTag.innerText = 'Admin panel';
		   topLinksTag.append(adminPanelTag);
		   var pTag = document.createElement('p');
		   pTag.innerText = '|';
		   topLinksTag.appendChild(pTag);
		}
	</script>
    ```

	This discloses the admin panel URL.

1. Visit the `/admin-g3r4bx` page
1. Use the `Delete` button to delete user `carlos`




# User role controlled by request parameter

<https://portswigger.net/web-security/access-control/lab-user-role-controlled-by-request-parameter>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab has an admin panel at `/admin`, which identifies administrators using a forgeable cookie.
>
>> Solve the lab by accessing the admin panel and using it to delete the user `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener`, see the link to the admin panel and learn that it is at `/admin`
1. Intercept the GET request for `/admin` (no need to have signed in as `wiener` first)
1. Change the value of the `Admin` cookie to `true`

    ```
	Cookie: session=Q2WXA5OZTMhnyvgD62NY8RwwfqJAzhS5; Admin=true
    ```

1. Forward the request and you get the admin panel
1. Use the `Delete` button to delete user `carlos`
1. Capture the GET request and do the same (change `Admin` to `true`)
1. Forward the request and `carlos` is deleted




# User role can be modified in user profile

<https://portswigger.net/web-security/access-control/lab-user-role-can-be-modified-in-user-profile>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab has an admin panel at `/admin`. It's only accessible to logged-in users with a `roleid` of 2.
>
>> Solve the lab by accessing the admin panel and using it to delete the user `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Change email using the field on the `My account` page
1. The response to this request includes `"roleid":1` in the JSON response body
1. The POST request itself contains JSON, but only `"email":"<input>"`
1. Add `"roleid":2` to the POST request's JSON:

    ```
	{"email":"test@test.com", "roleid": 2}
    ```

1. Send this request. The response includes `"roleid":2`
1. Visit the `/admin` page. We have roleid of 2 so the admin interface is displayed
1. Use the `Delete` button to delete user `carlos`




# URL-based access control can be circumvented

<https://portswigger.net/web-security/access-control/lab-url-based-access-control-can-be-circumvented>

Completed: TKTK




# Method-based access control can be circumvented

<https://portswigger.net/web-security/access-control/lab-method-based-access-control-can-be-circumvented>

Completed: TKTK




# User ID controlled by request parameter

<https://portswigger.net/web-security/access-control/lab-user-id-controlled-by-request-parameter>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab has a horizontal privilege escalation vulnerability on the user account page.
> 
>> To solve the lab, obtain the API key for the user `carlos` and submit it as the solution.
> 
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Visit the account homepage
1. Notice the URL ends in: `my-account?id=wiener`
1. Replace `?id=wiener` with `?id=carlos` and visit this page
1. User `carlos`'s My Account page is displayed
1. Copy their API key and paste into the solution field




# User ID controlled by request parameter, with unpredictable user IDs

<https://portswigger.net/web-security/access-control/lab-user-id-controlled-by-request-parameter-with-unpredictable-user-ids>

Completed: 2022-12-02


## Summary

TKTK


## Objective

> This lab has a horizontal privilege escalation vulnerability on the user account page, but identifies users with GUIDs.
>
>> To solve the lab, find the GUID for `carlos`, then submit his API key as the solution.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Visit the user My Account home page
1. The URL contains the following:

    ```
	/my-account?id=71069a5c-f4c2-4f3c-a810-5b1899eddb64
    ```

1. The `id` parameter is hexadecimal values of varying lengths separated by hyphens, in the form `8-4-4-4-12`. This is therefore an IETF RFC4122 UUID, with the components representing `time_low - time_mid - time_hi_and_version - clock_seq_hi_and_clock_seq_lo - node`
1. Logging out and back in again does not change the UUID parameter in any way
1. Find a blog post written by `carlos`. Clicking on the author's name reveals the following in the requested URL:

    ```
	GET /blogs?userId=36d4768b-3c5e-46a3-9a70-2a1dc18b46b3 HTTP/1.1
    ```

1. Copy the `userID` value from this URL and replace the `id` parameter value in the `/my-account` request/URL
1. Visiting this page takes you to `carlos`'s profile home page
1. Steal the API key and submit it




# User ID controlled by request parameter with data leakage in redirect

<https://portswigger.net/web-security/access-control/lab-user-id-controlled-by-request-parameter-with-data-leakage-in-redirect>

Completed: 2022-12-02


## Summary

TKTK


## Objective

> This lab contains an access control vulnerability where sensitive information is leaked in the body of a redirect response.
>
>> To solve the lab, obtain the API key for the user `carlos` and submit it as the solution.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in with `wiener:peter`
1. Go to `/my-account`
1. Notice that the GET request contains the username as a parameter:

    ```
	GET /my-account?id=wiener HTTP/1.1
    ```

1. Change the `id` value to `carlos` and visit that page
1. You are immediately redirected back to the login page, but the HTTP history shows that the GET request received a 302 (redirect, found) response
1. In the body of this redirect response, the profile page of user `carlos` is revealed, including their API key
1. Copy and submit the API key




# User ID controlled by request parameter with password disclosure

<https://portswigger.net/web-security/access-control/lab-user-id-controlled-by-request-parameter-with-password-disclosure>

Completed: 2022-12-02


## Summary

TKTK


## Objective

> This lab has user account page that contains the current user's existing password, prefilled in a masked input.
>
>> To solve the lab, retrieve the administrator's password, then use it to delete `carlos`.
>
> You can log in to your own account using the following credentials: `wiener:peter`


## Solution

1. Log in as `wiener:peter`
1. Go to `my-account` homepage, notice that the request is for:

    ```
	GET /my-account?id=wiener HTTP/1.1
    ```

1. Change `wiener` to `administrator` and visit their user profile page
1. View page source, inspecting the prefilled change password field
1. This gives you the password to the `administrator` account
1. Log in as `administrator`
1. Visit `/admin`
1. Use the `Delete` button to delete user `carlos`




# Insecure direct object references

<https://portswigger.net/web-security/access-control/lab-insecure-direct-object-references>

Completed: 2022-12-06


## Summary

TKTK


## Objective

> This lab stores user chat logs directly on the server's file system, and retrieves them using static URLs.
>
>> Solve the lab by finding the password for the user `carlos`, and logging into their account.


## Solution

1. Go to the "Live chat" page (`/chat`)
1. Click "View transcript". A file called `2.txt` will be downloaded containing this chat's history
1. In Burp HTTP History, you can see that the download request was a GET request to `/download-transcript/2.txt`
1. Point the browser to this download URL, replacing `2.txt` with `1.txt`
1. Read the downloaded file, which contains user `carlos`'s password
1. Log in as `carlos`




# Multi-step process with no access control on one step

<https://portswigger.net/web-security/access-control/lab-multi-step-process-with-no-access-control-on-one-step>

Completed: TKTK




# Referer-based access control

<https://portswigger.net/web-security/access-control/lab-referer-based-access-control>

Completed: TKTK

